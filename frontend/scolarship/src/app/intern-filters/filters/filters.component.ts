import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  ascending: boolean = true;
  dateFormats: string[] = [
    'short',
    'medium',
    'long',
    'full',
    'shortDate',
    'mediumDate',
    'longDate',
    'fullDate',
    'shortTime',
    'mediumTime',
    'longTime',
    'fullTime'
  ];
  selectedDateFormat: string = 'short';
  @Output() ascendingChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() dateFormatChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  sendAscending(): void {
    this.ascendingChanged.emit(this.ascending);
  }

  sendDateFormat(): void {
    this.dateFormatChanged.emit(this.selectedDateFormat);
  }
}
