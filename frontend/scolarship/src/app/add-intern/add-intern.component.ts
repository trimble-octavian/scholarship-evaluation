import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Intern } from '../models/intern';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss']
})
export class AddInternComponent implements OnInit {
  addInternForm: FormGroup;
  intern: Intern;
  internId: string;

  constructor(private internService: InternService,
    private router: Router,
    private route: ActivatedRoute) {
      this.route.params.subscribe(args => this.internId = args['id']);
    }

  ngOnInit(): void {
    this.initForm();
    if(this.internId !== undefined)
    {
      this.internService.getIntern(this.internId).subscribe(intern => {
        this.intern = intern; 
        this.addInternForm.patchValue({
        'firstName': this.intern.firstName,
        'lastName': this.intern.lastName,
        'dateOfBirth': this.intern.dateOfBirth
      })
    });
    }  
  }

  onSubmit() {
    const intern: Intern = {
      'firstName': this.addInternForm.controls['firstName'].value,
      'lastName': this.addInternForm.controls['lastName'].value,
      'dateOfBirth': this.addInternForm.controls['dateOfBirth'].value
    };
    this.internId === undefined ? 
    this.internService.addIntern(intern).subscribe(() => {
      this.router.navigate(['']);
    }) :
    this.internService.updateIntern(this.internId, intern).subscribe(() => {
      this.router.navigate(['']);
    });
  }

  private initForm() {
    this.addInternForm = new FormGroup({
      'firstName': new FormControl(null, Validators.required),
      'lastName': new FormControl(null, Validators.required),
      'dateOfBirth': new FormControl(null , Validators.required)
    })
  }
}
