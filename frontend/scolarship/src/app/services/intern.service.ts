import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Intern } from '../models/intern';

@Injectable({
  providedIn: 'root'
})
export class InternService {
  readonly baseUrl = "https://localhost:44369/api/interns/";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient) {

  }

  getInterns(): Observable<Intern[]> {
    return this.httpClient
    .get<Intern[]>(
      this.baseUrl,
      this.httpOptions
    );
  }

  getIntern(internId: string): Observable<Intern> {
    return this.httpClient
    .get<Intern>(
      this.baseUrl+internId,
      this.httpOptions
    );
  }

  addIntern(intern: Intern){
    return this.httpClient
    .post(
    this.baseUrl,
    intern,
    this.httpOptions
    );
  }

  updateIntern(internId: string, intern: Intern){
    return this.httpClient
    .put(
    this.baseUrl+internId,
    intern,
    this.httpOptions
    );
  }

  deleteIntern(internId: string){
    return this.httpClient
    .delete(
    this.baseUrl+internId,
    this.httpOptions
    );
  }
  
}
