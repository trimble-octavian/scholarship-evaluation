import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Intern } from '../models/intern';
import { InternService } from '../services/intern.service';

@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.scss']
})
export class InternComponent implements OnInit {
  interns: Intern[] = [];
  dateFormat: string;

  constructor(private internService: InternService) { }

  ngOnInit(): void {
    this.getInterns();
  }

  ngOnChanges(): void {
    this.getInterns();
  }

  getInterns()
  {
    this.internService.getInterns().subscribe(
      (interns: Intern[]) => {
        this.interns = interns;
      });
  }

  sortInterns(ascendingParam: Boolean)
  {
    this.interns.sort((a: Intern, b: Intern) => {
      return ascendingParam ? +(a.firstName > b.firstName) : +(a.firstName < b.firstName);
    })
  }

  getDateFormat(dateFormat: string)
  {
    this.dateFormat = dateFormat;
  }

  deleteIntern(internId: string) {   
    this.internService.deleteIntern(internId).subscribe(() => this.getInterns());
  }
}
