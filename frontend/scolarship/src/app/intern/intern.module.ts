import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InternComponent } from './intern.component';

import {MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import { MatCardModule } from "@angular/material/card";
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { FiltersComponent } from '../intern-filters/filters/filters.component';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    InternComponent,
    FiltersComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatCardModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule
  ],
  exports: [
    InternComponent,
    FiltersComponent
  ]
})
export class InternModule { }
