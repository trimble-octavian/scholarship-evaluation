export interface Intern {
    id?: string,
    dateOfBirth: Date,
    firstName: string,
    lastName: string
}