import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInternComponent } from './add-intern/add-intern.component';
import { InternComponent } from './intern/intern.component';

const routes: Routes = [
  { path: 'add-intern', component: AddInternComponent },
  { path: 'edit-intern/:id', component: AddInternComponent },
  { path: '', component: InternComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
