﻿using scholarship.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scholarship.Services.Interface
{
    public interface IInternService : ICollectionService<Intern>
    {
    }
}
