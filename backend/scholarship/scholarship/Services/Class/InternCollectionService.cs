﻿using scholarship.Models;
using scholarship.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scholarship.Services.Class
{
    public class InternCollectionService : IInternService
    {
        public static List<Intern> _interns = new List<Intern>
        {
            new Intern { ID = new Guid("00000000-0000-0000-0000-000000000001"), FirstName = "Octavian", LastName = "Niculescu", DateOfBirth=new DateTime(2001,01,01)},
            new Intern { ID = new Guid("00000000-0000-0000-0000-000000000002"), FirstName = "Andrei", LastName = "Popescu", DateOfBirth=new DateTime(2002,01,01)},
            new Intern { ID = new Guid("00000000-0000-0000-0000-000000000003"), FirstName = "Calin", LastName = "David", DateOfBirth=new DateTime(2003,01,01)},
        };
        public bool Create(Intern model)
        {
            _interns.Add(model);
            return true;
        }

        public bool Delete(Guid id)
        {
            int index = _interns.FindIndex(intern => intern.ID == id);
            if (index == -1)
            {
                return false;
            }
            _interns.RemoveAt(index);
            return true;
        }

        public Intern Get(Guid id)
        {
            return (from intern in _interns
                          where intern.ID == id
                          select intern).FirstOrDefault();
        }

        public List<Intern> GetAll()
        {
            return _interns;
        }

        public bool Update(Guid id, Intern model)
        {
            int index = _interns.FindIndex(intern => intern.ID == id);
            if (index == -1)
            {
                return false;
            }
            model.ID = id;
            _interns[index] = model;
            return true;
        }
    }
}
