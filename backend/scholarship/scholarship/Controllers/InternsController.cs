﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using scholarship.Models;
using scholarship.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scholarship.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InternsController : ControllerBase
    {
        IInternService _internService;
        public InternsController(IInternService internService)
        {
            _internService = internService ?? throw new ArgumentNullException(nameof(internService));
        }

        [HttpGet]
        public IActionResult GetInterns()
        {
            return Ok(_internService.GetAll());
        }

        [HttpGet("{Id}", Name = "GetIntern")]
        public IActionResult GetIntern([FromRoute] Guid Id)
        {
            Intern? intern = _internService.Get(Id);
            if(intern == null)
            {
                return NotFound();
            }
            return Ok(intern);
        }

        [HttpPost]
        public IActionResult AddIntern([FromBody] Intern intern)
        {
            intern.ID = Guid.NewGuid();
            _internService.Create(intern);
            return CreatedAtRoute("GetIntern", new { ID = intern.ID }, intern);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateIntern([FromBody] Intern intern, Guid id)
        {
            if (intern == null)
            {
                return BadRequest("Intern cannot be null");
            }
            _internService.Update(id, intern);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteNote(Guid id)
        {
            bool deleted = _internService.Delete(id);
            if (deleted == false)
            {
                return NotFound("Intern cannot be found");
            }
            return Ok();
        }
    }
}
